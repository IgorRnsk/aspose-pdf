package com.rstyle.sib.aspose.pdf;

import aspose.pdf.*;
import com.aspose.pdf.*;
import org.junit.Before;
import org.junit.Test;

import java.io.IOException;

import static org.junit.Assert.*;

public class AsposePdf421Test {

    private AsposePdf421 pdf;

    @Before
    public void setUp() throws Exception {
        pdf = new AsposePdf421("Test");
    }

    @Test
    public void setDefaultFont() {

    }

    @Test
    public void createNewPdf() {
        assertEquals(0, pdf.createNewPdf());
    }

    @Test
    public void createFileSecurity() {
    }

    @Test
    public void setPrivilege() {
    }

    @Test
    public void addSection() {
    }

    @Test
    public void setMarginLeft() {
    }

    @Test
    public void setMarginRight() {
    }

    @Test
    public void setMarginTop() {
    }

    @Test
    public void setMarginBottom() {
    }

    @Test
    public void setIsLandscape() {
    }

    @Test
    public void text() {
    }

    @Test
    public void addTextSegment() {
    }

    @Test
    public void setTextSegmentFont() {
    }

    @Test
    public void addText() {
        Section section = createSection();
        Text text = pdf.Text("Hello, world!");
        pdf.addText(section, text);
        pdf.save("TestAddText.pdf");
    }

    @Test
    public void addTextMargin() {
    }

    @Test
    public void setFontSize() {
    }

    @Test
    public void setIsTrueTypeFontBold() {
    }

    @Test
    public void setTextColor() {
    }

    @Test
    public void setTextAligment() {
    }

    @Test
    public void addTableInSection() {
        Section section = createSection();
        pdf.addTableInSection(section, "Table Header");
        pdf.save("addTableInSection.pdf");
    }

    @Test
    public void setFontNameInRow() {
    }

    @Test
    public void addTableInCell() {
    }

    @Test
    public void setColumnSpan() {
    }

    @Test
    public void setColumnWidth() {
    }

    @Test
    public void addRow() {
        Table table = createTable("Test addRow");
        Row row = pdf.addRow(table);
        pdf.addCell(row, "C1");
        pdf.addCell(row, "C2");
        pdf.addCell(row, "C3");
        pdf.save("AddRowTest.pdf");
    }

    @Test
    public void addCell() {
    }

    @Test
    public void addCell2() {
    }

    @Test
    public void setMarginCell() {
    }

    @Test
    public void setAlignment() {
        Cell cell = new Cell();
        pdf.setAlignment(cell, "");
    }

    @Test
    public void setVerticalAligment() {
    }

    @Test
    public void setCellBorderColor() {
    }

    @Test
    public void setCellBorder() {
    }

    @Test
    public void save() {
    }

    @Test
    public void addWaterMarkText() {
    }

    @Test
    public void addWaterMarkImage() {
        Section section = createSection();

        pdf.addWaterMarkImage(100.0f, 1.0f, 0.25f, "/IMAG0153.jpg");

        Text text = pdf.Text("Hello, world!");
        pdf.addText(section, text);

        pdf.save("watermark.pdf");
    }

    @Test
    public void addImageInSection() {
        Section section = createSection();
        pdf.addImageInSection(section, 1.0f, 1.0f, 0.25f, "/IMAG0153.jpg");
        Text text = pdf.Text("Hello, world!");
        pdf.addText(section, text);

        pdf.save("addimageinsection.pdf");
    }

    @Test
    public void addImageInCell() {
    }

    @Test
    public void getPagesNumber() {
        Table table = createTable("Test PageNumber");
        for (int i = 0; i < 200; i++) {
            Row row = pdf.addRow(table);
            pdf.addCell(row, fillCell(i, 1));
            pdf.addCell(row, fillCell(i, 2));
            pdf.addCell(row, fillCell(i, 3));
        }
        assertEquals(0, pdf.getPagesNumber());
        pdf.save("Test PageNumber.pdf");
        assertEquals(4, pdf.getPagesNumber());
    }

    private String fillCell(int row, int cell) {
        return String.format("R%sC%s", row, cell);
    }


    private Section createSection() {
        pdf.createNewPdf();
        return pdf.addSection();
    }

    private Table createTable(String test_addRow) {
        Section section = createSection();
        return pdf.addTableInSection(section, "Table");
    }


}