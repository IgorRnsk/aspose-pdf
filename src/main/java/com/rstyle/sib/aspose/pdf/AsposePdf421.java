package com.rstyle.sib.aspose.pdf;

import aspose.pdf.AlignmentType;
import aspose.pdf.BorderInfo;
import aspose.pdf.BorderSide;
import aspose.pdf.Cell;
import aspose.pdf.Color;
import aspose.pdf.FloatingBox;
import aspose.pdf.Image;
import aspose.pdf.ImageFileType;
import aspose.pdf.MarginInfo;
import aspose.pdf.Pdf;
import aspose.pdf.Row;
import aspose.pdf.Section;
import aspose.pdf.Security;
import aspose.pdf.Segment;
import aspose.pdf.Table;
import aspose.pdf.Text;
import aspose.pdf.TextInfo;
import aspose.pdf.VerticalAlignmentType;
import com.aspose.pdf.License;
import com.aspose.pdf.facades.DocumentPrivilege;
import com.aspose.pdf.facades.PdfFileSecurity;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.net.URL;
import java.util.Properties;


/**
 * @author IgorR
 * @since 27/10/2014
 */
@SuppressWarnings({"SameReturnValue", "unused"})
public class AsposePdf421 {

    //private static final Logger LOG = Logger.getLogger(AsposePdf421.class);
private static final Logger LOG = LoggerFactory.getLogger(AsposePdf421.class);

    private static final String CONFIG_PROPERTIES = "/config.properties";
    // properties
    private static final String PROP_LIC_FILE = "lic.file";
    private static final String PROP_FONT_DIR = "font.dir";
    private static final String PROP_FONT_NAME = "font.name";
    private static final String PROP_FONT_SIZE = "font.size";
    private static final String PROP_BORDER_WIDTH = "border.width";
    private static final float WATERMARKS_BOX_SIZE_H = 400.0F;
    private static final float WATERMARKS_BOX_SIZE_W = 400.0F;
    // parameters
    private final Float DEFAULT_BORDER_WIDTH;
    private final String DEFAULT_FONT_DIRECTORY;
    private final String DEFAULT_FONT_NAME;
    private final Float DEFAULT_FONT_SIZE;

    private static final License license = new License();
    private Pdf pdf;
    @SuppressWarnings("FieldCanBeLocal")
    private Section section;
    private int pagesNumber = 0;
    private PdfFileSecurity pdfFileSecurity;
    //private Properties properties = new Properties();

    private String font;
    private float fontSize;

    public AsposePdf421(String comment) {
        LOG.info("Create AsposePdf object with comment:  " + comment);
        Properties properties = new Properties();

        try {
            InputStream is = AsposePdf421.class.getResourceAsStream(CONFIG_PROPERTIES);
            properties.load(is);
            String licenseFile = properties.getProperty(PROP_LIC_FILE);

            is = AsposePdf421.class.getResourceAsStream(licenseFile);
            license.setLicense(is);
            LOG.debug("The license is read");
        } catch (Exception e) {
            LOG.error("The license is invalid", e);
            throw new RuntimeException(e);
        }

        DEFAULT_FONT_DIRECTORY = properties.getProperty(PROP_FONT_DIR);
        DEFAULT_FONT_NAME = properties.getProperty(PROP_FONT_NAME);
        DEFAULT_FONT_SIZE = Float.valueOf(properties.getProperty(PROP_FONT_SIZE));
        DEFAULT_BORDER_WIDTH = Float.valueOf(properties.getProperty(PROP_BORDER_WIDTH));

    }

    /**
     * @return errCode
     */
    public int createNewPdf() {
        int err = 0;
        try {
            if (pdf == null) {
                LOG.debug("Create new Pdf object");
                pdf = new Pdf();
            }
            pdf.setTruetypeFontDirectoryPath(DEFAULT_FONT_DIRECTORY);
            pdf.setSecurity(new Security());
            pdf.getSecurity().setIsCopyingAllowed(false);
        } catch (Error e) {
            err = 1;
            LOG.error("Create pdf:", e);
        }

        font = DEFAULT_FONT_NAME;
        fontSize = DEFAULT_FONT_SIZE;

        return pdf == null ? 1 : err;
    }

    public Pdf getPdf() {
        return pdf;
    }

    /**
     * @param inputFile  file in
     * @param outputFile file out
     * @return err code
     */
    public int createFileSecurity(String inputFile, String outputFile) {
        int err = 0;
        this.pdfFileSecurity = new PdfFileSecurity();
        this.pdfFileSecurity.setInputFile(inputFile);
        this.pdfFileSecurity.setOutputFile(outputFile);
        if (this.pdfFileSecurity == null) {
            err = 1;
        }
        return err;
    }

    /**
     * @param privilege
     * @return
     */
    public int setPrivilege(int privilege) {
        int err = 0;
        int maskRead = 1;
        int maskPrint = 2;
        int maskCopy = 4;

        try {
            DocumentPrivilege documentPrivilege;
            if ((privilege & maskRead) == 1) {
                documentPrivilege = DocumentPrivilege.getScreenReaders();
            } else {
                documentPrivilege = DocumentPrivilege.getAllowAll();
                if ((privilege & maskPrint) == 2) {
                    documentPrivilege.setAllowPrint(false);
                }

                if ((privilege & maskCopy) == 4) {
                    documentPrivilege.setAllowCopy(false);
                }
            }

            this.pdfFileSecurity.setPrivilege(documentPrivilege);
            this.pdfFileSecurity.close();
        } catch (Exception e) {
            LOG.error("setPrivilege()", e);
            err = 1;
        }
        return err;
    }

    /**
     * @param defaultFont Font name
     */
    public void setDefaultFont(String defaultFont) {
        font = defaultFont;
    }

    public void setFont(String font) {
        this.font = font;
    }

    public void setFontSize(float fontSize) {
        this.fontSize = fontSize;
    }


    /**
     * @return
     */
    public Section addSection() {
        this.section = this.pdf.getSections().add();
        return this.section;
    }

    /**
     * @param s      - Section
     * @param margin
     */
    public void setMarginLeft(Object s, float margin) {
        ((Section) s).getPageInfo().getMargin().setLeft(margin);
    }

    public void setMarginRight(Object s, float margin) {
        ((Section) s).getPageInfo().getMargin().setRight(margin);
    }

    public void setMarginTop(Object s, float margin) {
        ((Section) s).getPageInfo().getMargin().setTop(margin);
    }

    public void setMarginBottom(Object s, float margin) {
        ((Section) s).getPageInfo().getMargin().setBottom(margin);
    }

    @SuppressWarnings("SameReturnValue")
    public int setIsLandscape(Object s, boolean isLandscape) {
        ((Section) s).setIsLandscape(isLandscape);
        return 0;
    }

    /**
     * @param str
     * @return
     */
    public Text Text(String str) {
        Text text = new Text(str);
        text.getTextInfo().setFontName(font);
        text.getTextInfo().setFontSize(fontSize);
        return text;
    }

    /**
     * @param text Text
     * @param str  string
     * @return
     */
    public Segment addTextSegment(Object text, String str) {
        return ((Text) text).getSegments().add(str);
    }

    public void setTextSegmentFont(Object segment, String fontName, float textFontSize, Boolean isFontBold, Boolean isFontItalic) {
        ((Segment) segment).getTextInfo().setFontName(fontName);
        ((Segment) segment).getTextInfo().setFontSize(textFontSize);
        ((Segment) segment).getTextInfo().setIsTrueTypeFontBold(isFontBold);
        ((Segment) segment).getTextInfo().setIsTrueTypeFontItalic(isFontItalic);
    }

    @SuppressWarnings("SameReturnValue")
    public int addText(Object section, Object text) {
        ((Section) section).getParagraphs().add((Text) text);
        return 0;
    }

    public int addTextMargin(Object section, Object text, float left, float top, float right, float bottom) {
        ((Text) text).getMargin().setLeft(left);
        ((Text) text).getMargin().setTop(top);
        ((Text) text).getMargin().setRight(right);
        ((Text) text).getMargin().setBottom(bottom);
        ((Section) section).getParagraphs().add((Text) text);
        return 0;
    }

    public int setFontSize(Object text, float textFontSize) {
        ((Text) text).getTextInfo().setFontSize(textFontSize);
        return 0;
    }

    public int setIsTrueTypeFontBold(Object text, boolean isTrueTypeBold) {
        ((Text) text).getTextInfo().setIsTrueTypeFontBold(isTrueTypeBold);
        return 0;
    }

    public int setTextColor(Object text, String color) {
        ((Text) text).getTextInfo().setColor(new Color(color));
        return 0;
    }

    public void setTextAligment(Object text, String textAlignment) {
        ((Text) text).getTextInfo().setAlignment(this.chooseAlignmentType(textAlignment));
    }

    public Table addTableInSection(Object section, String title) {
        Table table = new Table();
        ((Section) section).getParagraphs().add(table);
        table.setTitle(title);
        return table;
    }

    public int setFontNameInRow(Object row, String fontName) {
        ((Row) row).getDefaultCellTextInfo().setFontName(fontName);
        return 0;
    }

    public Table addTableInCell(Object cell) {
        Table table = new Table();
        table.setColumnWidth(0, 100.0F);
        table.setColumnWidth(1, 100.0F);
        ((Cell) cell).getDefaultCellTextInfo().setFontName(font);
        ((Cell) cell).setAlignment(AlignmentType.Center);
        ((Cell) cell).getParagraphs().add(table);
        return table;
    }

    public int setColumnSpan(Object cell, int spanCols) {
        ((Cell) cell).setColumnsSpan(spanCols);
        return 0;
    }

    public int setColumnWidth(Object table, int column, float width) {
        ((Table) table).setColumnWidth(column, width);
        return 0;
    }

    public Row addRow(Object table) {
        Row row = new Row((Table) table);
        ((Table) table).getRows().add(row);
        return row;
    }

    public Cell addCell(Object row, String value) {
        Cell cell = ((Row) row).getCells().add(value);
        cell.setBorder(new BorderInfo(BorderSide.All.getValue(), DEFAULT_BORDER_WIDTH));
        cell.setAlignment(AlignmentType.Center);
        cell.setVerticalAlignment(VerticalAlignmentType.Center);
        return cell;
    }

    public Cell addCell2(Object row, float fontSize, boolean isTrueTypeBold, String value, boolean isTrueTypeItalic) {
        TextInfo textInfo = new TextInfo();
        textInfo.setFontName(font);
        textInfo.setFontSize(fontSize);
        textInfo.setIsTrueTypeFontBold(isTrueTypeBold);
        textInfo.setIsTrueTypeFontItalic(isTrueTypeItalic);
        Cell cell = ((Row) row).getCells().add(value, textInfo);
        cell.setBorder(new BorderInfo(BorderSide.All.getValue(), DEFAULT_BORDER_WIDTH));
        cell.setAlignment(AlignmentType.Center);
        cell.setVerticalAlignment(VerticalAlignmentType.Center);
        return cell;
    }

    public int setMarginCell(Object cell, float leftMargin, float topMargin, float rightMargin, float bottomMargin) {
        int idxParagraph = 0;
        int err = 0;
        MarginInfo marginInfo = ((Cell) cell).getParagraphs().getParagraph(idxParagraph).getMargin();

        try {
            if (leftMargin > 0.0F) {
                marginInfo.setLeft(leftMargin);
            }

            if (rightMargin > 0.0F) {
                marginInfo.setRight(rightMargin);
            }

            if (topMargin > 0.0F) {
                marginInfo.setTop(topMargin);
            }

            if (bottomMargin > 0.0F) {
                marginInfo.setBottom(bottomMargin);
            }

            ((Cell) cell).getParagraphs().getParagraph(idxParagraph).setMargin(marginInfo);
        } catch (Exception e) {
            LOG.error("setMarginCell", e);
            err = 1;
        }
        return err;
    }

    private AlignmentType chooseAlignmentType(String strAlignmentType) {
        AlignmentType alignmentType;

        if (strAlignmentType.toUpperCase().equals("CENTER")) {
            alignmentType = AlignmentType.Center;
        } else if (strAlignmentType.toUpperCase().equals("RIGHT")) {
            alignmentType = AlignmentType.Right;
        } else if (strAlignmentType.toUpperCase().equals("LEFT")) {
            alignmentType = AlignmentType.Left;
        } else if (strAlignmentType.toUpperCase().equals("FULLJUSTIFY")) {
            alignmentType = AlignmentType.FullJustify;
        } else if (strAlignmentType.toUpperCase().equals("JUSTIFY")) {
            alignmentType = AlignmentType.Justify;
        } else {
            alignmentType = AlignmentType.Center;
        }

        return alignmentType;
    }

    /**
     * @param cell
     * @param cellAlignmentType
     */
    public void setAlignment(Object cell, String cellAlignmentType) {
        ((Cell) cell).setAlignment(this.chooseAlignmentType(cellAlignmentType));
    }

    public void setVerticalAlignment(Object cell, String verticalAlignment) {
        if (verticalAlignment.toUpperCase().equals("CENTER")) {
            ((Cell) cell).setVerticalAlignment(VerticalAlignmentType.Center);
        } else if (verticalAlignment.toUpperCase().equals("BOTTOM")) {
            ((Cell) cell).setVerticalAlignment(VerticalAlignmentType.Bottom);
        } else if (verticalAlignment.toUpperCase().equals("TOP")) {
            ((Cell) cell).setVerticalAlignment(VerticalAlignmentType.Top);
        } else {
            ((Cell) cell).setVerticalAlignment(VerticalAlignmentType.Center);
        }

    }

    @SuppressWarnings("WeakerAccess")
    public void setCellBorderColor(Object cell, String strBorderSide, float borderWidth, String color) {
        int borderSide;
        if (strBorderSide.toUpperCase().equals("NONE")) {
            borderSide = BorderSide.None.getValue();
        } else if (strBorderSide.toUpperCase().equals("ALL")) {
            borderSide = BorderSide.All.getValue();
        } else if (strBorderSide.toUpperCase().equals("RIGHT")) {
            borderSide = BorderSide.Right.getValue();
        } else if (strBorderSide.toUpperCase().equals("LEFT")) {
            borderSide = BorderSide.Left.getValue();
        } else if (strBorderSide.toUpperCase().equals("TOP")) {
            borderSide = BorderSide.Top.getValue();
        } else if (strBorderSide.toUpperCase().equals("BOTTOM")) {
            borderSide = BorderSide.Bottom.getValue();
        } else {
            borderSide = BorderSide.None.getValue();
        }

        ((Cell) cell).setBorder(new BorderInfo(borderSide, borderWidth, new Color(color)));
    }

    public void setCellBorder(Object cell, String borderSide, float borderWidth) {
        String color = "Black";
        this.setCellBorderColor(cell, borderSide, borderWidth, color);
    }

    public int save(String outFileName) {
        LOG.debug("save");
        int errCode = 0;
        try {
            File file = new File(outFileName);
            FileOutputStream fileOutputStream = new FileOutputStream(file);
            this.pdf.save(fileOutputStream);
            pagesNumber = pdf.getPagesNumber();
            LOG.info(String.format("The file \"%s\" is saved, the number of pages %s", outFileName, pagesNumber));
        } catch (Exception e) {
            LOG.error("Error during save pdf", e);
            errCode = 1;
        }
        return errCode;
    }

    public boolean addWaterMarkText(float leftCorner, float topCorner, String textWaterMark) {
        Text text = new Text(textWaterMark);
        text.getTextInfo().setFontSize(18.0F);
        text.getTextInfo().setColor(new Color("Gray"));
        FloatingBox watermark = new FloatingBox(300.0F, 50.0F);
        watermark.setLeft(leftCorner);
        watermark.setTop(topCorner);
        watermark.getParagraphs().add(text);
        pdf.getWatermarks().Add(watermark);
        return true;
    }

    public boolean addWaterMarkImage(float leftCorner, float topCorner, float imageScale, String filePathImage) {
        LOG.debug("call addWaterMarkImage: " + filePathImage);
        LOG.debug("corner: " + leftCorner + " " + topCorner + " " + imageScale);

        boolean result = false;
        String filePath = getAbsolutePath(filePathImage);

        if (existsFile(filePath)) {
            Image image = new Image();
            image.getImageInfo().setFile(filePath);
            image.getImageInfo().setImageFileType(ImageFileType.Jpeg);
            image.setImageScale(imageScale);
            FloatingBox watermark = new FloatingBox(WATERMARKS_BOX_SIZE_H, WATERMARKS_BOX_SIZE_W);
            watermark.setLeft(leftCorner);
            watermark.setTop(topCorner);
            watermark.getParagraphs().add(image);
            pdf.getWatermarks().Add(watermark);
            result = true;
        } else {
            LOG.error("No such image file: " + filePath);
        }
        return result;
    }

    public boolean addImageInSection(Object section, float leftCorner, float topCorner, float imageScale, String filePathImage) {
        LOG.debug("call addImage: " + filePathImage);
        LOG.debug("corner:" + leftCorner + " " + topCorner + " " + imageScale);

        boolean result = false;
        String filePath = getAbsolutePath(filePathImage);

        if (existsFile(filePath)) {
            Image image = new Image();
            image.getImageInfo().setFile(filePath);
            image.getImageInfo().setImageFileType(ImageFileType.Jpeg);
            image.setImageScale(imageScale);
            ((Section) section).getParagraphs().add(image);
            result = true;
        } else {
            LOG.error("No such image file: " + filePath);
        }
        return result;
    }

    public boolean addImageInCell(Object cell, float leftCorner, float topCorner, float imageScale, String filePathImage) {
        LOG.debug("call addImage: " + filePathImage);
        LOG.debug("corner:" + leftCorner + " " + topCorner + " " + imageScale);

        boolean result = false;
        String filePath = getAbsolutePath(filePathImage);

        if (this.existsFile(filePath)) {
            Image image = new Image();
            image.getImageInfo().setFile(filePath);
            image.getImageInfo().setImageFileType(ImageFileType.Jpeg);
            image.setImageScale(imageScale);
            ((Cell) cell).getParagraphs().add(image);
            result = true;
        } else {
            LOG.error("No such image file: " + filePath);
        }
        return result;
    }

    /**
     * @return Count pages
     */
    public int getPagesNumber() {
        return pagesNumber;
    }

    /**
     * @param filePath
     * @return
     */

    private boolean existsFile(String filePath) {
        File file = new File(filePath);
        return file.exists();
    }

    private String getAbsolutePath(String filePathImage) {
        String path = filePathImage;
        if (path != null && path.startsWith("/")) {
            URL resource = AsposePdf421.class.getClassLoader().getResource(path.substring(1));
            if (resource != null) {
                path = resource.getPath();
            }
        }
        LOG.debug("Image file path: " + path);
        return path;
    }

}
